/**
 * 这是一个模拟的定位sdk，用来定期返回位置更新,仅用作参考。
 * */
// var _mockdata = [
//     {x:12619619.5302,y:2621837.57344,angle:-18.7316},
// {x:12619621.8002,y:2621844.3059,angle:70.5842},
// {x:12619618.4709,y:2621845.486,angle:70.5842},
// {x:12619613.8585,y:2621847.1211,angle:70.5843},
// {x:12619609.5198,y:2621848.65904,angle:70.5843},
// {x:12619605.7921,y:2621849.9804,angle:70.5843},
// {x:12619601.4842,y:2621851.5075,angle:-19.6214},
// {x:12619602.78,y:2621855.16286,angle:-19.6214},
// {x:12619604.1663,y:2621859.07372,angle:-19.6214},
// {x:12619605.6941,y:2621863.38373,angle:-19.6214},
// {x:12619607.4449,y:2621868.32275,angle:-19.6214},
// {x:12619608.8883,y:2621872.3947,angle:-19.6214},
// {x:12619610.6506,y:2621877.3664,angle:69.5213},
// {x:12619606.0665,y:2621879.0882,angle:69.5213}
// ];

var _mockdata = [
  { x: 13389083.2514, y: 3532691.1065, angle: 20}
  // { x: 13389085.1922, y: 3532699.8402, angle: 0 },
  // { x: 13389061.3803, y: 3532699.0191, angle: 0 },
  // { x: 13389039.2105, y: 3532699.3177, angle: 0 },
  // { x: 13389037.2697, y: 3532716.7101, angle: 0 },
  // { x: 13389029.6559, y: 3532717.3819, angle: 0 }
];
var _callback;
export var _update;
// var _updateInternal;
var _freq = 800;
var _index = 0;

/**
 * 模拟更新位置，按照时间间隔更新位置信息。
 * @param {*} 回调函数
 */
export function updateLocation(cb) {
  _callback = function() {
    var _data;
    if (_index > _mockdata.length - 1) {
      _index = 0;
    }
    _data = _mockdata[_index];
    _index++;
    return cb(_data);
  };
  _update = setInterval(_callback, _freq);
}

/**
 * 停止位置更新
 */
export function stopUpdateLocation() {
  clearInterval(_update);
  console.log("update stoped.");
}
